const SIZE_SMALL = 'SIZE_SMALL';
const SIZE_LARGE = 'SIZE_LARGE';
const SIZES = {
    SIZE_SMALL: {
        price: 50,
        calories: 20
    },
    SIZE_LARGE: {
        price: 100,
        calories: 40
    },
};
const STUFFING_CHEESE = 'STUFFING_CHEESE';
const STUFFING_SALAD = 'STUFFING_SALAD';
const STUFFING_POTATO = 'STUFFING_POTATO';
const STUFFINGS = {
    STUFFING_CHEESE: {
        price: 10,
        calories: 20,
    },
    STUFFING_SALAD: {
        price: 20,
        calories: 5,
    },
    STUFFING_POTATO: {
        price: 15,
        calories: 10,
    },
};
const TOPPING_SPICE = 'TOPPING_SPICE';
const TOPPING_MAYO = 'TOPPING_MAYO';
const TOPPINGS = {
    TOPPING_SPICE: {
        price: 15,
        calories: 0,
    },
    TOPPING_MAYO: {
        price: 20,
        calories: 5,
    }
};
class HamburgerExceptions{
    constructor(message) {
        this.message = message
    }
}

class Hamburger {
    constructor(size, stuffing) {
        try{
            if(!size){
                throw new HamburgerExceptions('Size is not a given')
            }
            else if(!Object.keys(SIZES).includes(size)){
                throw new HamburgerExceptions(`Invalid size ${size}`)
            }
            if(!stuffing){
                throw new HamburgerExceptions('Stuffing is not a given')
            }
            else if(!Object.keys(STUFFINGS).includes(stuffing)){
                throw new HamburgerExceptions(`Invalid size ${stuffing}`)
            }
        } catch(error){
            console.error(`${error.name}: ${error.message}`)
        }
       this._stuffing = stuffing;
       this._size = size;
       this._topping = [];
    }

    addTopping(topping) {
        this._topping.push(topping)
    }
    removeTopping(topping) {
        return this._topping = this._topping.filter(elem => elem == topping);
    }

    get getToppings(){
        return this._topping
    }

    get getSize(){
        return this._size
    }

    get getStuffing(){
        return this._stuffing
    }

    calculatePrice(){
        const calcPrice = this._topping.map(elem => TOPPINGS[elem].price);
        calcPrice.push(SIZES[this._size].price, STUFFINGS[this._stuffing].price);
        return calcPrice.reduce((item, price) => item + price )
    }
    calculateCalories(){
        const calcCalories = this._topping.map(elem=> TOPPINGS[elem].calories);
        calcCalories.push(SIZES[this._size].calories, STUFFINGS[this._stuffing].calories);
        return calcCalories.reduce((item, price)=> item + price);
    }
}

const hamburger = new Hamburger(SIZE_LARGE, STUFFING_CHEESE)
console.log(hamburger);

hamburger.addTopping(TOPPING_MAYO)
console.log(hamburger);

console.log(hamburger.calculatePrice());
console.log(hamburger.calculateCalories());