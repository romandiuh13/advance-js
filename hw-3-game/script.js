// 1.Создать функцию, которая создает и возвращает HTML-таблицу в столько-то столбцов
// и строк. Функция должна вернуть DOM-элемент table со всей разметкой внутри.
// У нее будут 2 аргумента:
// -количество столбцов в таблице;
// -количество ячеек в таблице;
// 2. Создать функцию, которая случайным образом выбирает целое число из диапазона
// и возвращает его. Функция будет получать 2 аргумента:
// -нижняя  граница диапазона;
// -верхняя граница диапазона.
//     Пример: getRandomFromRange(1, 30) - вернет случайное целое число от 1 до 30.
// 3. Усложнить предыдущий пример: написать функцию, которая получает массив объектов,
// и случайным образом выбирает один из них по индексу,
// после чего удаляет этот объект из массива и возвращает его.
// 5. Запускать вышеописанную функцию через 1500 мс.
//     Усложнить пункт 1: функция должна создавать указанное число DOM-элементов - ячеек,
//     и возвращать 2 значения: ссылку на DOM-элемент - таблицу,
//     и массив всех ее ячеек в виде массива DOM-элементов.
// 6. Переписать функцию из пункта 3, передавая в качестве аргумента массив DOM-ячеек,
// полученный из функции 5, и после удаления элемента из массива
// присваивать этой ячейке класс “active” (красит ячейку в синий цвет).
// 7. Усложнить предыдущий пример: запускать каждые 1500 мс функцию из пункта 6,
// при это сохраняя вырезаную ячейку во внешнюю переменную.
// 8. Повесить на таблицу обработчик click, который работает так:
//     если нажали на ячейку, он проверяет, есть ли у нее класс “active”.
//     если есть - заменяет его на класс “success” (красит ее в зеленый цвет).
// 9. Усложнить функцию из пункта 7: перед выбором новой случайной ячейки,
// проверять старую. И если у нее есть класс “active”,
// заменять его на “failed” (красит ее в красный цвет).
// 10. Усложнить функцию из пункта 9 - если массив пустой
// - запуск функции каждые 1500 мс останавливается.
// 11. Создать две внешних переменных, которые будут хранить счет.
// 1 - счет игрока, вторая - счёт компьютера.
// Усложнить пример из пункта 10: если у предыдущей ячейки
// есть класс active - одно очко добавляется к счету компьютера, если нет - игрока.
// 12. Усложнить функцию из пункта 10: если после увеличения счета он
// у игрока/компьютера стал больше 50 - игра останавливается
// и в консоль выводится победитель и его счет.

    class Game{
    constructor(tr, td) {
        this.tr = tr;
        this.td = td;
        this._array = [];
    }
   render(){
        const tableList = document.createElement('table');
        tableList.className = 'table'

        for (let i=0; i < this.tr; i++){
            const tr = document.createElement('tr');
            tr.className = 'table-tr'
            tableList.append(tr)

            for(let i =0; i <this.td; i++){
                const td = document.createElement('td');
                td.className = 'table-td'
                td.textContent = 'work'
                tr.append(td)
            }
       }

       const tableDiv = document.querySelector('.table-game')
       tableDiv.append(tableList)


   }

    getCells(){
        const allCells = document.querySelectorAll('.table-td')
         return this._array = Array.prototype.slice.call(allCells);

    }

   getRandomFromRange(array) {
       const rand = Math.floor(Math.random() * array.length);
       const randomCell =  array[rand];
       console.log(randomCell);
       const indexCell = array.indexOf(randomCell, 0)
       console.log(indexCell);
       const spliceCell = array.splice(randomCell, 1)
       spliceCell.forEach(item => item.classList.add('active'))
       console.log(spliceCell)
        }

}

const game = new Game(3,5);
game.render();
console.log(game.getCells());
// console.log(game._array);
game.getRandomFromRange(game._array);
// console.log(this._array);
